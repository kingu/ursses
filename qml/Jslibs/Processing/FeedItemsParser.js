WorkerScript.onMessage = function parseItems(message) {
	var item = message.item;
	var channelData = message.channelData;
	var channelDomain = (""+channelData["feedUrl"]).match(/https?:\/\/([^\/]+)/).pop();
	try {
			//console.log("channel items :" + JSON.stringify(item));
			item["chImageUrl"] = channelData["imageUrl"];
			item['domain'] = channelDomain
			item["channel"] = channelData["titleText"] ? channelData["titleText"] : channelDomain ;
			try {
				//If no date then try and  get the date from the title
				if(!item["updated"]) {
					item["updated"] = Date.parse(
						item["title"].match(/(\d{4}[\-./]\d\d[\-./]\d\d|\d\d[\-./]\d\d[\-./]\d{4})/) ? 
							item["title"].match(/(\d{4}[\-./]\d\d[\-./]\d\d|\d\d[\-./]\d\d[\-./]\d{4})/).pop() :
							item["description"].match(/(\d{4}[\-./]\d\d[\-./]\d\d|\d\d[\-./]\d\d[\-./]\d{4})/).pop()
					);
				}
			} catch(e) {/*If we can't get a date we cant get a date...*/}
			item["updateDate"] = (new Date(Date.parse(item["updated"]))).toDateString();
			item["itemData"] =  JSON.parse(JSON.stringify(item));
			WorkerScript.sendMessage({"item":item});
	} catch(e){
		console.log("Cloudn't parse item : "+ i + "for :"+ channelData["feedUrl"]);
	}
}

