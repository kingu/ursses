var apiData = {
	'searchDomain' : {
		'host' : 'https://feedsearch.dev/api/v1/search?url='
	}
}
function searchDomain(domain, callback) {
	var xhr = new XMLHttpRequest();
	xhr.open("GET",apiData['searchDomain']['host']+domain);
	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			callback(JSON.parse(xhr.responseText));
		}
	};
	xhr.send();
}
