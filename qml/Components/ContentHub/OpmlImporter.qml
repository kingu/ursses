import QtQuick 2.9
import QtQuick.XmlListModel 2.0

Item {
	function importOPML(transfer) {
		console.log("Get Uknown import type with the following items :");
		for ( var i = 0; i < transfer.items.length; i++ ) {
			var  urlStr = ""+(""+transfer.items[i].url);
			if ( transfer.items[i] && transfer.items[i].url && urlStr.match(/\.(opml|xml)/) ) {
				//TODO  fix this ugly  code!!
				var xhr = new XMLHttpRequest();
				xhr.open("GET",transfer.items[i].url)
				mainLayout.primaryPage.header.message.text = i18n.tr("Importing : %1").arg(urlStr.replace(/^.*\//,""));
				xhr.onreadystatechange = function() {
					if (xhr.readyState == XMLHttpRequest.DONE) {
						//console.log(xhr.responseText);
						var  opmlReader = opmlReaderComponent.createObject(null,{xml:xhr.responseText});
						opmlReader.statusChanged.connect(function() { 
							if (opmlReader.status == XmlListModel.Ready) {
								var incubator = mainLayout.addPageToNextColumn(mainLayout.primaryPage, 
																	Qt.resolvedUrl("../UI/ImportFeedsPage.qml"),{"feedsModel": opmlReader});
								incubator.onStatusChanged = function(status) {
									if (status == Component.Ready) {
										var page = incubator.object;
										page.feedsToImport.connect(function(feeds){
											mainLayout.removePages(page);
											for(var idx=0; idx < feeds.length ; idx++) {
												mainEventBillboard.addFeed(feeds[idx]);
											}
										});
									}
								}
							}
						});
					}
				}
				xhr.send();
			} else {
				console.log("Received unsupported item : " + JSON.stringify(transfer.items[i]));
				mainLayout.primaryPage.header.message.text = i18n.tr("Received unsupported file : %1").arg(urlStr.replace(/^.*\//,""));
			}
		} 
	}
	Component {
		id:opmlReaderComponent
		OPMLFeedEnteries {
		}
	}
}
