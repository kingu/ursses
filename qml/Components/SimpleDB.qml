import QtQuick 2.9
import U1db 1.0 as U1db

Item {
	id:_feedCache
	property var dbName:"darkeye-ursses-main-db"
	property alias db:_mainDbInstance
	
	function getDoc(key) {
		queryDocsByValue.query = [ key ];
		if(queryDocsByValue.results.length) {
			for(var i in queryDocsByValue.documents) {
				var cachedDoc = _mainDbInstance.getDoc(queryDocsByValue.documents[i]);
				if(cachedDoc && cachedDoc.value) {
					return cachedDoc.value;
				}
			}
		}
		return null;
	}
	
	function getDocs(filter) {
		filter = filter ? filter : "*"
		var results = [];
		queryDocsByValue.query = [ filter ];
		if(queryDocsByValue.results.length) {
			for(var i in queryDocsByValue.documents) {
				var foundDoc = _mainDbInstance.getDoc(queryDocsByValue.documents[i]);
				if(foundDoc && foundDoc.value) {
					results.push(foundDoc.value);
				}
			}
		}
		return results;
	}
	
	function putDoc(value, category) {
		category = category ? category : 'feed'
		db.putDoc({"value": value,"category":category});
	}
	
	
	U1db.Database {
		id:_mainDbInstance
		path: dbName
	}

	U1db.Index {
		id:sourceIndex
		database:_mainDbInstance
 		name:"sourceIndex"
		expression: [ "value" ]
	}
	U1db.Query {
		id:queryDocsByValue
		index: sourceIndex
		query: ["*"]
	}
}
 /** Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



