
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls

import "../"

Page {
	id:_importfeeds
	
	signal feedsToImport(var feeds);
	
	property alias feedsModel: _feedsList.model
	
	clip:true
	
	header: PageHeader {
		id:header
		title:i18n.tr("Select Feeds to import")
		trailingActionBar  { 
				numberOfSlots: 4
				actions : [
					Action {
						text : i18n.tr("Ok")
						iconName : "ok"
						onTriggered : {
							var items = [];
							//console.log( _feedsList.ViewItems.selectedIndices);
							for(var i=0; i < _feedsList.ViewItems.selectedIndices.length; i++) {
								items.push( _feedsList.model.get(_feedsList.ViewItems.selectedIndices[i]).url);
							}

							_importfeeds.feedsToImport(items);
						}
					},
					Action {
						enabled:false
					},
					Action {
						text : i18n.tr("Select None")
						iconName : "select-none"
						enabled : _feedsList.ViewItems.selectedIndices.length > 0
						onTriggered : {
							_feedsList.clearSelection()
						}
					},
					Action {
						text : i18n.tr("Select All")
						iconName : "select"
						enabled : _feedsList.ViewItems.selectedIndices.length < _feedsList.model.count
						onTriggered : {
							_feedsList.selectAll()
						}
					}
				]
		}
	}

	UbuntuListView {
		id:_feedsList
		anchors {
			top:header.bottom
			left:parent.left
			right:parent.right
			bottom:parent.bottom
		}
		currentIndex: -1
		Component.onCompleted : {
			_feedsList.ViewItems.selectMode = true
		}
		model:null
		delegate: ListItem {
			height:units.gu(8)
			ListItemLayout {
				title.text : model.title
				subtitle.text : model.url
				summary.text : model.description
				
			}
			color : selected ? theme.palette.selected.background : "transparent";
			onClicked: {
                selected = !selected
			}
		}
		
		function clearSelection() {
			ViewItems.selectedIndices = []
		}

		function selectAll() {
			var tmp = []

			for (var i=0; i < model.count; i++) {
				tmp.push(i)
			}

			ViewItems.selectedIndices = tmp
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


