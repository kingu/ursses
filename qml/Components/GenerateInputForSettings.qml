
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls

Column {
	id:_inputColumn
	clip:true
	property var inputsObject: null
	property var inputsObjectValues: null
	
	property var propertiesToIgnore : ["objectName","fileName","category","mainFeedSortAsc"];
	property var fieldsSelection : {}
	property var presentableNames : {}
	property var containers: {"default" : _regularSettingsContainer}
	
	onInputsObjectChanged : {
		//remove all current childrens
		_inputColumn.clearCntainers(containers);
		_inputColumn.clearCntainers([_regularSettingsContainer]);
		
		//generate inputs
		for(var i in inputsObject) {
			if(propertiesToIgnore.indexOf(i) > -1) { continue; }
			//console.log(i,inputsObject[i],typeof(inputsObject[i]));
			var defaultProps = {
				"title.text": presentableNames[i] ? presentableNames[i]['title'] : i.toString(),
				"summary.text": presentableNames[i] ? presentableNames[i]['description'] : "",
				"value":inputsObject[i],
				"callback" : (function(propId){ return function(val) { _inputColumn.inputsObject[propId] = val;}; })(i)
			};
			var parentInstance = presentableNames[i] && 
								presentableNames[i]["container"] && 
								_inputColumn.containers[presentableNames[i]["container"] ] ?
									_inputColumn.containers[presentableNames[i]["container"] ] : 
									(_inputColumn.containers['default'] ? 
										_inputColumn.containers['default'] : 
										_defaultSettingsContainer );
			if(fieldsSelection[i]) {
				defaultProps['possibleValues'] = fieldsSelection[i];
				selectValueInputComponent.createObject(parentInstance,defaultProps);
				continue;
			}
			switch(typeof(inputsObject[i])) {
				case 'number' : 
					if(presentableNames[i]['minimumValue'] && presentableNames[i]['maximumValue'] ) {
						defaultProps['minimumValue'] = presentableNames[i]['minimumValue'];
						defaultProps['maximumValue'] = presentableNames[i]['maximumValue'];
						sliderInputComponent.createObject(parentInstance,defaultProps);
					} else {
						numberInputComponent.createObject(parentInstance,defaultProps);
					}
					break;
				case 'string' : 
					stringInputComponent.createObject(parentInstance,defaultProps);
					break;
				case 'boolean' : 
					booleanInputComponent.createObject(parentInstance,defaultProps);
					break;
			}
			
		}
	}
	
	Row {
		anchors {
			left:parent.left
			right:parent.right
		}
		Column {
			width:parent.width
			id:_defaultSettingsContainer
		}
	}
	
	//-------------------------------- Components ------------------------------------
	
	Component {
		id:numberInputComponent
		ListItemLayout {
			id: numberInput
			property var value: 0
			property var callback: null
			title.text: i18n.tr("Number") 
			summary.wrapMode: Text.WordWrap


			TextField {
				SlotsLayout.position: SlotsLayout.Trailing
				width: units.gu(15)
				inputMethodHints:Qt.ImhFormattedNumbersOnly
				text: numberInput.value
				onTextChanged: {
					if(callback) {
						callback(parseFloat(text));
					}
				}
			}
		}
	}
	
	Component {
		id:sliderInputComponent
		ListItemLayout {
			id: silderInput
			property var value: 0
			property var minimumValue: 0.33
			property var maximumValue: 1
			property var callback: null
			title.text: i18n.tr("Slider") 
			summary.wrapMode: Text.WordWrap

			Slider {
				SlotsLayout.position: SlotsLayout.Trailing
				width: units.gu(15)
 				value: silderInput.value
				minimumValue: silderInput.minimumValue
				maximumValue: silderInput.maximumValue
				onValueChanged: {
					if(callback) {
						callback(parseFloat(value));
					}
				}
				function formatValue(v) { return Math.round(v*10,3)/10;}
			}
		}
	}
	
	Component {
		id:stringInputComponent
		ListItemLayout {
			id: stringInput
			property string value: ""
			property var callback: null
			title.text: i18n.tr("String")
			summary.wrapMode: Text.WordWrap

			TextField {
				SlotsLayout.position: SlotsLayout.Trailing
				width: units.gu(15)
				text:stringInput.value
				onTextChanged: {
					if(callback) {
						callback(text);
					}
				}
			}
		}
	}
	
	Component {
		id:booleanInputComponent
		ListItemLayout {
			id: booleanInput
			property bool value: false
			property var callback: null
			title.text: i18n.tr("Boolen")
			summary.wrapMode: Text.WordWrap

			CheckBox {
				SlotsLayout.position: SlotsLayout.Trailing
				width: units.gu(5)
				checked: booleanInput.value
				onCheckedChanged: {
					if(callback) {
						callback(checked);
					}
				}
			}
		}
	}
	
	Component {
		id:selectValueInputComponent
		ListItemLayout {
			id: selectValueInput
			property alias value: _comboBtn.text
			property alias possibleValues: _valueList.model
			property var callback: null
			title.text: i18n.tr("Select Value")
			summary.wrapMode: Text.WordWrap

			ComboButton {
				id:_comboBtn
				SlotsLayout.position: SlotsLayout.Trailing
				text: ""
				width: units.gu(15)
				ListView {
					id:_valueList
					model:[]
					delegate: ListItem {
						height: units.gu(5)
						ListItemLayout {
							title.text:modelData
						}
						onClicked: {
							callback(modelData);
							_comboBtn.expanded = false;
							_comboBtn.text = modelData;
						}
					}
				}
			}
		}
	}
	
	//--------------------------------------- Function ------------------------
	function clearCntainers(containers) {
		for(var containerIdx in containers) {
			for(var i = containers[containerIdx].children.length; i > 0 ; i--) {
				containers[containerIdx].children[i-1].destroy()
			}
		}
	}
}
/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


