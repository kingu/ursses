import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls

import "../Components"
import "../Components/UI"

Page {
	id:_settingsPage
	
	header: PageHeaderWithBottomText {
		id:header
		title:i18n.tr("Settings")
		leadingActionBar {
			actions: [
				Action {
					name : i18n.tr("back")
					iconName : "back"
					onTriggered : {
						mainLayout.removePages(_settingsPage)
					}
				}
			]
		}
		trailingActionBar {
			actions: [
				Action {
					name : i18n.tr("Accounts")
					iconName : "account"
					onTriggered : {
						mainLayout.addPageToNextColumn(_settingsPage, Qt.resolvedUrl("ManageAccountsPage.qml"),{})
					}
				},
				Action {
					name : i18n.tr("Feeds")
					iconName : "view-list-symbolic"
					onTriggered : {
						mainLayout.addPageToNextColumn(_settingsPage, Qt.resolvedUrl("ManageFeedsPage.qml"),{ feedsModel:root.urls })
					}
				},
				Action {
					name : i18n.tr("About")
					iconName : "info"
					onTriggered : {
						mainLayout.addPageToNextColumn(_settingsPage, Qt.resolvedUrl("Information.qml"),{})
					}
				}
			]
		}
	}
	Flickable {
		flickableDirection: Flickable.VerticalFlick
		clip:true
		anchors {
				top:header.bottom
				left:parent.left
				right:parent.right
				bottom:parent.bottom
			}
			contentHeight: settingsUI.height
			
		GenerateInputForSettings {
			id:settingsUI
			anchors {
				left:parent.left
				right:parent.right
			}
			inputsObject:appSettings
			containers: {
				"default" : _regularSettingsContainer,
				"advanced" : _advancedSettingsContainer
			}
			
			fieldsSelection : {
				"mainFeedSortField" :  ["updated","channel","author","titleText"],
				"mainFeedSectionField" :  ["updateDate","channel","author"]
			}
			
			presentableNames : { return {
				showDescInsteadOfWebPage : {
					title: i18n.tr("Show content instead of linked page"),
					description :i18n.tr("Shoud we just show the feed entry content instead of navigating to the related website")
				},
				showSections : {
					title : i18n.tr("Show section name"),
					description : i18n.tr("Seperates entries by Sections")
				},
				itemsToLoadPerChannel : {
					container : "advanced",
					title : i18n.tr("Item to load per feed"),
					description : i18n.tr("How much enteries to load per feed source")
				},
				mainFeedSortField : {
					container : "advanced",
					title : i18n.tr("Sorting field name"),
					description : i18n.tr("What form the feed entry field will be used for sorting.")
				},
				mainFeedSectionField : {
					title : i18n.tr("Section field name"),
					description : i18n.tr(" ")
				},
				webBrowserDefaultZoom : {
					container : "advanced",
					minimumValue:0.5,
					maximumValue:3,	
					title : i18n.tr("Web browser default zoom"),
					description : i18n.tr("The zoom value to set the internal web browser to.")
				},
				updateFeedEveryXMinutes : {
					container : "advanced",
					title : i18n.tr("Update feeds every"),
					description : i18n.tr("The amount of time (in minutes) to wait between automatic feeds update.")
				},
				openFeedsExternally : {
					title : i18n.tr("Open feeds URL Externally"),
					description : i18n.tr("Open feeds URL using external browser instead of using the internal browser view.")
				},
				swipeBetweenEntries :  {
					title : i18n.tr("Swipe next/Previous story"),
					description : i18n.tr("Move between stories using swipe to change to the next/previous story.")
				},
				feedCacheTimeout :  {
					container : "advanced",
					title : i18n.tr("Feed Cache timeout"),
					description : i18n.tr("How often should  we clean old entries in the cache? (in Minutes).")
				},
				webViewBaseOpacity :  {
					container : "advanced",
					minimumValue:0.33,
					maximumValue:1,
					title : i18n.tr("Web View default Opacity"),
					description : i18n.tr("The  default blending opacity for web view")
				}
			}; }
			
			Row {
				anchors {
					left:parent.left
					right:parent.right
				}
				Column {
					width:parent.width
					id:_regularSettingsContainer
				}
			}
			Text {
				topPadding:units.gu(5)
				leftPadding:units.gu(2)
				text :i18n.tr("Advance Settings")
				color: theme.palette.normal.backgroundText
				font.weight:Font.ExtraBold
			}
			Row {
				anchors {
					left:parent.left
					right:parent.right
				}
				Column {
					width:parent.width
					id:_advancedSettingsContainer
				}
			}
		}
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

