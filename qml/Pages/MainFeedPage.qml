
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import QtQuick.XmlListModel 2.0
import QtQml.Models 2.2

import "../Components"
import "../Components/UI"
import "../Jslibs/rssAPI.js" as RssAPI


Page {
	id:_mainFeed
	
	clip:true
	
	property var model : []
	property var refreshing:  cachedHttpRequestInstance.waitingForResults;
	property var currentSearch : null
	
	
	header: PageHeader {
		id:header
	}
	
	Component.onCompleted: {
		_mainFeed.updateFeed();
	}
	
	onRefreshingChanged: if(!refreshing) {
		//listSortingTimer.restart();
	}
	
	//------------------------------ Components -----------------------------
	
	Component {
		id:channelComponent
		RssChannel {
		}
	}
	
	Component {
		id:channelItemsComponent
		RssChannelItems {
			id:channelItemsComponentObj
		}
	}
	
	WorkerScript {
		id:feedItemsParser
		source : "../Jslibs/Processing/FeedItemsParser.js"
		
		onMessage :  (message) => {
			var item = message.item
			feedList.model.append(item);
		}
	}

	//---------------------------------------- -----------------------------
	
	// Timer  used to allow  for multiple updates that  result in a single sorting
	Timer {
		id:listSortingTimer
		interval:1000
		repeat: false
		running:false
		onTriggered : {
			console.log("Timer Sorting..")
			feedList.model.sort();
		}
	}


	function updateFeed() {
		if( !_mainFeed.model || _mainFeed.model.length ==  0 ||  _mainFeed.refreshing ||  listSortingTimer.running ) {
			return;
		}

		console.log("updating the feeds..");
		feedList.model.clear();

		for(var i in _mainFeed.model) {
			var url = _mainFeed.model[i];
			cachedHttpRequestInstance.send(url, {"url": url});
		}
	}
	
	function searchFor(searchText) {
		var startIdx = feedList.currentIndex;
		_mainFeed.currentSearch = searchText;
		for(var i= 0; i < feedList.model.count; i++) {
			var adjedIdx =(i+startIdx) % feedList.model.count;
			var item = feedList.model.get( adjedIdx );
			if(_mainFeed.itemMatchSearch(item,searchText)) {
				if(startIdx == feedList.currentIndex) {
					feedList.currentIndex = adjedIdx;
				}
			} 
		}
		
	}
	
	function itemMatchSearch(item, search) {
		if(!search) return true;
		
		var regexp = new RegExp(search,'i');
		return  !regexp || (item.titleText.match(regexp) ||  item.description.match(regexp) ||
				item.channel.match(regexp)  || item.content.match(regexp)) 
	}
	
	function clearSearch() {
		_mainFeed.currentSearch = "";
	}

	
	Feed {
		id:feedList
		
		anchors {
			topMargin:header.height
			fill:parent
		}

		model: FeedsModel {
				id:feedModel
		}
		highlightFollowsCurrentItem:true
		highlightMoveDuration: 250

		pullToRefresh {
			enabled: true
			refreshing:  feedList.model.count == 0  || listSortingTimer.running || _mainFeed.refreshing
			onRefresh: _mainFeed.updateFeed()
		}
		
		section.property :appSettings.showSections ?  appSettings.mainFeedSectionField : ""
        section.criteria: ViewSection.FullString
        section.labelPositioning: ViewSection.InlineLabels

        section.delegate: ListItem {
            height: sectionHeader.implicitHeight + units.gu(2)
            Label {
                id: sectionHeader
                text: section
                font.weight: Font.Bold
                anchors {
                    left: parent.left
                    leftMargin: units.gu(2)
                    verticalCenter: parent.verticalCenter
                }
            }
        }
		Rectangle {
			anchors { 
				fill:parent
			}
			z:1
			opacity: 0.66
			visible:  _mainFeed.model.length  == 0 ||  feedList.model.count == 0  || listSortingTimer.running || _mainFeed.refreshing
			color: theme.palette.normal.background
			ProgressBar {
				anchors { 
					horizontalCenter:parent.horizontalCenter
					bottom:loadingLabel.top
					margins:units.gu(2)
				}
				indeterminate:true
				visible: _mainFeed.model.length > 0 
				
			}
			Label {
				id:loadingLabel
				anchors { 
					verticalCenter:parent.verticalCenter
					left:parent.left
					right:parent.right
					margins:units.gu(2)
				}
				text: _mainFeed.model.length > 0 ? i18n.tr("Loading feeds....") :
													i18n.tr("No Feeds yet... You can add feeds by clicking on the '+' sign in the top right.")
				wrapMode: Text.Wrap
				horizontalAlignment:Text.AlignHCenter
			}
		}
		
		delegate: FeedItem {
			//visible: _mainFeed.itemMatchSearch(itemData,_mainFeed.currentSearch)
			onClicked:{
				feedList.currentIndex = index;
				if(appSettings.openFeedsExternally && itemData.url) {
					Qt.openUrlExternally(itemData.url);
				} else {
					feedList.openEntryView({"model": itemData });
				}
			}
		}

		Timer {
			interval: appSettings.updateFeedEveryXMinutes*60000
			running: appSettings.updateFeedEveryXMinutes > 0
			repeat: true
			triggeredOnStart:false
			onTriggered: { 
				_mainFeed.updateFeed();
			}
		}
		
		//======== Feeds Functions ===========
		
		function openEntryView(pageData) {
			var incubator =mainLayout.addPageToNextColumn(	mainLayout.primaryPage, 
															Qt.resolvedUrl("EnteryViewPage.qml"),
															 pageData );
			incubator.onStatusChanged = function(status) {
				if (status == Component.Ready) {
				}
			}
		}
	}
	
	
	Connections {
		target:mainEventBillboard
		onNextStory: {
			console.log("Next story!",showDesc);
			feedList.currentIndex =  (feedList.currentIndex + 1) % feedList.count
			var item = feedList.model.get(feedList.currentIndex);
			feedList.openEntryView({"model": item,"descOrWeb":showDesc});
		}
		onPreviousStory: {
			console.log("Previous story!",showDesc);
			feedList.currentIndex =  (feedList.count + feedList.currentIndex - 1) % feedList.count
			var item = feedList.model.get(feedList.currentIndex);
			feedList.openEntryView({"model": item,"descOrWeb":showDesc});
		}
	}
	
	Connections {
		target :cachedHttpRequestInstance
		onResponseDataUpdated :	(response, data) => {
			try {
				var channel = channelComponent.createObject(null,{});
				channel.statusChanged.connect(function()  {
				if (channel.status == XmlListModel.Ready) {
						var channelData = channel.get(0);
					if(!channelData) {
						//Workaround for atom rss feeds
						channel.namespaceDeclarations = "declare default element namespace 'http://www.w3.org/2005/Atom';";
						channel.xml = response;
						channelData = {};
						channelData['isAtom'] = 1;
					}
					//console.log("channel :" + JSON.stringify(channelData));
					channelData['feedUrl'] = data.url;
					channelData['fullXml'] = response;
						//feedsParser.sendMessage({});
					loadChannelItems(channelData);
					}
				});
				channel.xml = response;
			} catch(e) {
				console.log("Failed to load channel :" + JSON.stringify(channel) + " got exception : " + e);
				
			}
		}
		onRequestError : (error,errorResult,channel) => {
			console.log("ERROR : Failed to load channel :" + error +" Got results : "+ errorResult);
		}
		
		function loadChannelItems(channelData) {
			var channelItems = channelItemsComponent.createObject(null,{});
			if(channelData['isAtom']) {
				channelItems.namespaceDeclarations = "declare default element namespace 'http://www.w3.org/2005/Atom';";
			}
			var parseChannelItems = function parseChannelItems() { 
				if (channelItems.status == XmlListModel.Ready) {
					for(var i=0; i < channelItems.count && i < appSettings.itemsToLoadPerChannel; i++) {
						try {
							var item = channelItems.get(i);
							feedItemsParser.sendMessage({"item":item,"channelData":channelData});
						} catch(e){
							console.log("Cloudn't parse item : "+ i + "for :"+channelData["feedUrl"]);
						}
					}
					if(channelItems.count) {
						listSortingTimer.restart();
					} else {
						_mainFeed.header.message.text = "Cloudn't parse channel : " + channelData["feedUrl"];
						console.log("Cloudn't parse channel : "+channelData["feedUrl"]/* + " XML : " + channelData['fullXml']*/);
						channelItems.statusChanged.disconnect(parseChannelItems);
						delete(channelItems);
						//console.log( channelData['fullXml']);
						
					}
				}
			};
			channelItems.statusChanged.connect(parseChannelItems);
			
			channelItems.xml = channelData['fullXml'];
			//channelItems.source = channelData['feedUrl'];
		}
	}
	// ======================================== Bottom edge ============================
	BottomEdge {
		 id:bookmarksBottomEdge
		 height: parent.height
		 hint.text:i18n.tr("Bookmarks")
		 hint.deactivateTimeout:5000
		 hint.status:BottomEdgeHint.Active
		 contentComponent:	ManageBookmarksPage {
			 id:bottomBookmarks
			 opacity: bookmarksBottomEdge.dragProgress
			 implicitHeight: bookmarksBottomEdge.height
			 implicitWidth: bookmarksBottomEdge.width
		 }
	}
}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

