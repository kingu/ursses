/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2019  eran <email>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.9
import U1db 1.0 as U1db

Item {
   id:_cachedHttpReq

   signal responseDataUpdated(var response, var id);
   signal requestError(var error, var errorResults, var id);

   property bool isResultJSON: true
   property int cachingTimeMiliSec: 600000 // 10 minutes
   property real recachingFactor: 0.9 // how much (percentage wise) of the caching time can pass before trying sending the response again.
   property bool waitingForResults: false
   property bool onlyRetrunFreshCache: true // Dont return cached values that are older then  the cachingTime
   property bool cachedResponseIsEnough: false // Dont return updated data that the  allready returned as  cahnged data
   property bool logActions: false
   property alias selfCleaningTimeMs: selfCleaningTimer.interval
   
   function send(url,id, getData,postData) {
	   var id = id ? id :  Math.random();
	   var requestURL = url + (getData ? "?" + _internal.mapJsonToRequest(getData) : "");
		if(logActions) console.log("CachedHttpRequest: retriving data for: "+ requestURL +" , associated id : "+id);
		waitingForResults = true;
		var cachedFactorPassed = _internal.retriveFromCache(requestURL,id);
		if( cachedFactorPassed < recachingFactor) {
			// we have the cached response no need to  query the server again
			return;
		}
		var cachedResponseReturned = cachedFactorPassed < 1 || !onlyRetrunFreshCache;



		if(logActions) console.log("CachedHttpRequest request URL:"+requestURL);

		//Send Request
		cachedHttpRequestWorker.sendMessage({ 	"requestURL": requestURL,
												"postData": postData,
												"id":id,
												"cachedResponseReturned": cachedResponseReturned,
												"logActions":logActions	})
		
		return id;
   }

   WorkerScript {
	   id:cachedHttpRequestWorker
	   source:"CachedHttpRequest.js"
	   onMessage: (message) => {
		   {
			waitingForResults = false;
			if (message.success) {
				try {
					if(_cachedHttpReq.isResultJSON) {
						console.log(JSON.stringify(message.http))
						var response = JSON.parse(message.responseText);
						if(response) {
							if(logActions) console.log("CachedHttpRequest: got response for: "+ requestURL +" , associated id : "+message.id);
							// Update DB with the new results and add the current timestamp to it
							var docId = cachedReqDbInstance.putDoc({"request":message.requestURL,
																	"response": message.responseText,
																	"timestamp":Date.now(),
																	"isResultJSON":_cachedHttpReq.isResultJSON},
															Qt.md5(message.requestURL));
							if(logActions) console.log("CachedHttpRequest: cached response to  :"+docId);
							if (!cachedResponseIsEnough || !message.cachedResponseReturned ) {
								responseDataUpdated(response, message.id);
							}
						}
					} else {
						if(logActions) console.log("CachedHttpRequest: got response for: "+ message.requestURL +" , associated id : "+message.id);
						// Update DB with the new results and add the current timestamp to it
						var docId = cachedReqDbInstance.putDoc({"request":message.requestURL,
																"response": message.responseText,
																"timestamp":Date.now(),
															   "isResultJSON":_cachedHttpReq.isResultJSON,
																},
														Qt.md5(message.requestURL));
						if(logActions) console.log("CachedHttpRequest: cached response at  :"+docId);
						// On returned results fire resultsUpdated
						if (!cachedResponseIsEnough || !message.cachedResponseReturned ) {
							responseDataUpdated(message.responseText, message.id);
						}
					}
				} catch (error) {
					console.log("CachedHttpRequest: got error when quering: "+ message.requestURL +" , associated id : "+message.id + " , Error : " + error);
					requestError(error, message.responseText, message.id);
				}
			} else {
				waitingForResults = false;
				requestError(message.event, message.responseText, message.id);
			}
		}
	   }
   }
   
   onResponseDataUpdated : {
	   waitingForResults = false;
   }
   
   onRequestError : {
		waitingForResults = false;
   }
	//---------------------------------------------------

	U1db.Database {
		id:cachedReqDbInstance
		path: "cached-requests-db"
	}

	U1db.Index {
		id:requestIndex
		database:cachedReqDbInstance
 		name:"requestIndex"
		expression: [ "request" ]
	}
	U1db.Query {
		id:getPreviousResponses
		index: requestIndex
		query: ["*"]
	}
	
	Timer {
		id:selfCleaningTimer
		interval: 300000
		triggeredOnStart:true
		running:true
		onTriggered: {
			_internal.cleanOldCachedDocs();
		}
	}

	//---------------------------------------------------

	QtObject {
	   id:_internal

		// check if we allread have a response to a give request/query in the cache and send an update if we have it.
	   function retriveFromCache(requestURL, id) {
			//check DB if theres a cache response for the requested URL
		   //console.log(requestURL)
			getPreviousResponses.query = [ requestURL ]
// 			console.log(getPreviousResponses.results.length)
			if(getPreviousResponses.results.length) {
				for(var i in getPreviousResponses.documents) {
					var prvResponse = cachedReqDbInstance.getDoc(getPreviousResponses.documents[i]);
					var howOld = Date.now() - prvResponse.timestamp;
					var response = prvResponse.isResultJSON ? JSON.parse(prvResponse.response) : prvResponse.response;
					//console.log(cachingTimeMiliSec, howOld)
					if( cachingTimeMiliSec > howOld ) {
						// If so send  resultsUpdated withthe stored getData
						if(logActions) console.log("CachedHttpRequest: Loading response for: "+ requestURL +" from cache" );
						 _cachedHttpReq.responseDataUpdated(response,id);
						return howOld/cachingTimeMiliSec;
					} else {						
						//If the response if too old delete it
						console.log("CachedHttpRequest: "+requestURL+" Timestamp too old : "+  prvResponse.timestamp +"  -> "+ howOld +"ms, to load from cache" );
						//This seems to be VERY slow so lets maybe just  do it on cleanOldCachedDocs?
						cachedReqDbInstance.deleteDoc(getPreviousResponses.documents[i]);
						if(!onlyRetrunFreshCache ) { 
							_cachedHttpReq.responseDataUpdated(response,id);
							return 1;
						}
					}
				}
			}
			return 1;
	   }
	   
	   function cleanOldCachedDocs() {
		   getPreviousResponses.query = [ "*" ]
			if(logActions)  console.log("CachedHttpRequest: cleanning old  documents...")
			if(getPreviousResponses.results.length) {
				for(var i in getPreviousResponses.documents) {
					var prvResponse = cachedReqDbInstance.getDoc(getPreviousResponses.documents[i]);
					var howOld = Date.now() - prvResponse.timestamp;
					
					if( cachingTimeMiliSec * 1.25 < howOld ) {
						if(logActions) console.log("CachedHttpRequest: Cleanning old  document from :"+prvResponse.request + " didn't updated for :" + howOld + "ms" );
						cachedReqDbInstance.deleteDoc(getPreviousResponses.documents[i]);
					}
				}
			}
			if(logActions)  console.log("CachedHttpRequest: cleanning done.");
	   }

		function mapJsonToRequest(json) {
		    var retStr="";
			for(var i in json) {
				if(typeof(json[i]) == "object") {
					retStr += i + "[]" + _internal.mapJsonToRequest(json[i]);
				} else {
					retStr += i + "=" + json[i];
				}
				retStr += "&";
			}

			return retStr;
	   }
	}
}
