WorkerScript.onMessage = function(message) {
		// Send request to the configured URL
		var http = new XMLHttpRequest();
		http.open("GET", message.requestURL);
// 		http.setRequestHeader('Content-type', 'text/xml; charset=utf-8')
		var textToRemove = [];
		http.onreadystatechange = function() {
			if (http.readyState === XMLHttpRequest.DONE && http.status === 200) {
				//HACK  this  is an hack to work around the fact that QML xhr  accumolates HTTP redirects body
				var response = http.responseText;
				for(var strToRemove of textToRemove) {
					response = response.replace(strToRemove,'');
// 					console.log("------" + message.requestURL+ "\n" + strToRemove)
				}
				
				WorkerScript.sendMessage({"success": true , "http" :http,"responseText":response,"id":message.id,"requestURL": message.requestURL, "cachedResponseReturned": message.cachedResponseReturned});
			} else {
				if(message.logActions) console.log("CachedHttpRequest:Worker: http readystate : "+ JSON.stringify(http.readyState) + "  HTTP status " + http.status);
				//HACK  this  is an hack to work around the fact that QML xhr  accumolates HTTP redirects body
				if(!(""+http.status).match(/^2\d+/) && http.response) {
					var response = http.response;
					for(var strToRemove of textToRemove) {
						response = response.replace(strToRemove,'');
					}
					textToRemove.push(response);
// 					console.log("++++++++++++" + message.requestURL)
				}
			}
		}

		// On Error send requestError signal
		http.onerror = function(event) {
			WorkerScript.sendMessage({"success": false , "http" :http,"responseText":http.responseText,"id":message.id,"requestURL": message.requestURL, "cachedResponseReturned": message.cachedResponseReturned, "event": event});
		}

		http.send(message.postData ? message.postData : null);
}
