
import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QControls
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import Ubuntu.Content 1.3
import QtWebEngine 1.7
import QtQuick.XmlListModel 2.0

import "Components/UI"
import "Components/ContentHub"
import "Components"
import "Pages"
import "helpers"
import "Jslibs/nextCloudAPI.js" as NextcloudAPI

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'darkeye.ursses'
    automaticOrientation: true
	anchorToKeyboard: true
	
	LayoutMirroring.enabled: false
    LayoutMirroring.childrenInherit: false

    width: units.gu(45)
    height: units.gu(75)
	
	theme.name: ""
    backgroundColor: theme.palette.normal.background
	property var urls :  [];
	property alias mainBillboard: mainEventBillboard
	
	onUrlsChanged : if(root.urls.length) {
		console.log("onUrlsChanged")
		mainBillboard.feedsChanged(root.urls);
		mainFeed.updateFeed();
		urssesMainDB.syncUrls();
	}
	
	Settings {
		id:appSettings
		property bool showDescInsteadOfWebPage: false
		property bool showSections: false
		property string mainFeedSectionField : "channel"
		property alias urls: root.urls
		property var bookedmarked : []
		property int itemsToLoadPerChannel : 7
		property int mainFeedSortAsc :Qt.DescendingOrder
		property string mainFeedSortField : "updated"
		property real webBrowserDefaultZoom : 1.0
		property int updateFeedEveryXMinutes : 5
		property bool openFeedsExternally: false
		property bool swipeBetweenEntries: true
		property int feedCacheTimeout: 10
		property real webViewBaseOpacity: 1.0
		property var nextCloudCreds :{"host":"","user":"","pass":"","accountId":false}
	}
	
	FeedCache {
		id:feedCacheDB
	}
	
	SimpleDB {
		id:urssesMainDB
		function syncUrls() {
			var dbUrls = urssesMainDB.getDocs();
			if(root.urls && root.urls.length) {
				if( (!dbUrls || !dbUrls.length || root.urls.length > dbUrls.length )  ) {
					console.log("Updating DB.");
					for(var idx in root.urls) {
						if(!urssesMainDB.getDoc(url[idx])) {
							urssesMainDB.putDoc(url[idx]);
						}
					}
				}
			} else if(dbUrls.length && !root.urls.length ) {
				root.urls = urssesMainDB.getDocs();
			}
		}
	}
	
	CachedHttpRequest {
		id:cachedHttpRequestInstance
		isResultJSON : false;
		logActions:false;
		onlyRetrunFreshCache:true;
		cachingTimeMiliSec: appSettings.feedCacheTimeout * 60000;
		recachingFactor: 0.25;
		cachedResponseIsEnough:true
	}
	
	WebEngineProfile {
			id:webProfileInstace
			persistentCookiesPolicy: WebEngineProfile.AllowPersistentCookies
			storageName: "ursses"
			httpCacheType: WebEngineProfile.DiskHttpCache
	}
		
	EventBillboard {
		id:mainEventBillboard
		onBookmarkFeed :{
			appSettings.bookedmarked.push(feed);
			appSettings.bookedmarked = appSettings.bookedmarked;
			NextcloudAPI.bookmarkFeed(feed.url)
		}
		onRemoveFeed:{
			for(var index=0; index< root.urls.length;index++) {
				if(feedUrl == root.urls[index]) {
					root.urls.splice(index,1);
					break;
				}
			}
			root.urls = root.urls;
		}
		onAddFeed: {
			if(root.urls.indexOf(feedUrl) < 0) {
				root.urls.push( feedUrl );
				root.urls = root.urls;
			}
		}
	}
	// -------------------------------------------- UI ----------------------------------
	
	AdaptivePageLayout {
		id:mainLayout
		anchors {
			fill:parent
		}
		asynchronous: true
		primaryPage : MainFeedPage {
			id:mainFeed
				model: root.urls
				header: PageHeaderWithBottomText {
					id: _header
					title: i18n.tr('Rsses')
					
					property var mode: "regular"
					property var searchHeader : Row {
						anchors {
							verticalCenter:parent.verticalCenter
							left:parent.left
							right:parent.right
						}
						id:contentsRow
						spacing:units.gu(0.5)
						Button {
							height:parent.height
							width:height
							color:theme.palette.normal.background
							iconName:"cancel"
							onClicked:{
								_header.mode = "regular";
								mainFeed.clearSearch();
							}
						}
						TextField {
							id:searchTextField
							width:parent.width - parent.height - units.gu(2)
							onTextChanged:if(text.length > 3) {
								mainFeed.searchFor(text);
							}
							onAccepted: {
								mainFeed.searchFor(text);
							}
							 Keys.onEscapePressed: {
								focus = false
								_header.mode = "regular";
								mainFeed.clearSearch();
							}
						}
					}
					leadingActionBar {
						actions : [
							Action {
								text : i18n.tr("uRsses")
								iconSource : Qt.resolvedUrl("../assets/logo.svg")
								onTriggered : {
									mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/Information.qml"),{})
								}
							}
						]
					}
					states : [
						State {
							name:"searchMode"
							when: _header.mode == "search"
							PropertyChanges { 
								target: _header;
								trailingActionBar.numberOfSlots : 2;
								contents: _header.searchHeader
							}
						}
					]
					trailingActionBar  { 
						numberOfSlots: 4
						actions : [
								Action {
										text : i18n.tr("Search")
										iconName : "search"
										onTriggered : {
											if(_header.mode ==  "regular" ) {
												_header.mode =   "search";
												searchTextField.focus = true;
											} else {
												mainFeed.searchFor(searchTextField.text)
											}
										}
									},
									Action {
										text : i18n.tr("Add Feed")
										iconName : "list-add"
										onTriggered : {
											mainLayout.addPageToNextColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/AddRssPage.qml"),{})
										}
									},
									Action {
										text : i18n.tr("Settings")
										iconName : "settings"
										onTriggered : {
											mainLayout.addPageToCurrentColumn(mainLayout.primaryPage, Qt.resolvedUrl("Pages/SettingsPage.qml"),{})
										}
									},
									Action {
										property bool isAscending : appSettings.mainFeedSortAsc == Qt.AscendingOrder;
										text : i18n.tr("Toggle Sort (%1) ").arg(isAscending ? i18n.tr("Ascending") : i18n.tr("Descending") )
										iconName : isAscending ? "up" : "down"
										onTriggered : {
											appSettings.mainFeedSortAsc = isAscending ?
																			Qt.DescendingOrder :
																			Qt.AscendingOrder;
										}
									}
									
								]
						
					}
				}
		}
	}
	
	// =============== Sync Next Cloud Feeds ===============
	function syncNextCloud() {
		mainLayout.primaryPage.header.message.text = i18n.tr("Synchronizing nextCloud");
		NextcloudAPI.getFeeds(appSettings.nextCloudCreds,function(feedsResult) {
			if(feedsResult && feedsResult.feeds) {
				var updated = false;// This should prevent reloading of the feeds when theres no need to.
				var nextCloudFeeds = [];
				for(var i in feedsResult.feeds) {
					if( feedsResult.feeds[i] && root.urls.indexOf(feedsResult.feeds[i].url) < 0) {
						root.urls.push(feedsResult.feeds[i].url);
						nextCloudFeeds.push(feedsResult.feeds[i].url);
						updated = true;
					}
				}
				
				if(updated) {
					root.urls = root.urls;
				}
				mainLayout.primaryPage.header.message.text = updated ?  i18n.tr("Synchronization done") :  i18n.tr("Synchronized");
			}
		});
	}
	
	Connections {
		//TODO : implement this, for some reason the add remove API fails with InternalError from the server...
		target:mainEventBillboard
		enabled:NextcloudAPI.isCredentialValid(appSettings.nextCloudCreds);
		onRemoveFeed : {
			console.log("Nextcloud remove feed for : "+ JSON.stringify(feedUrl));
			NextcloudAPI.removeFeed(appSettings.nextCloudCreds, feedUrl);
		}
		onAddFeed : {
			if(root.urls.indexOf(feedUrl) < 0) {
				console.log("Nextcloud add feed for : "+ JSON.stringify(feedUrl));
				NextcloudAPI.addFeed(appSettings.nextCloudCreds, feedUrl);
			}
		}
		onExportFeeds: {
			console.log("Exporting feeds");
			mainLayout.primaryPage.header.text.message = i18n.tr("Exporting feeds..."); 
		}
	}
	
	Timer {
		id:syncNextCloudTimer
		running: appSettings.nextCloudCreds.accountId !== undefined && appSettings.nextCloudCreds.encodedCreds !== undefined &&
				 appSettings.nextCloudCreds.accountId && appSettings.nextCloudCreds.encodedCreds
		interval: 30 * 60000
		triggeredOnStart: true
		repeat: true
		onTriggered: syncNextCloud();
	}
	
	//==================================  Sharing is caring ===========================
	property var urlMapping: {
		"^https?://" : function(url) {
			//if(pageStack && pageStack.currentPage && pageStack.currentPage.folderModel) {
				//pageStack.currentPage.folderModel.goTo(url);
			//}
			console.log("handling http:// for:"+url);
			for(var i in root.urls) {
				if(url == root.urls[i]) {
					console.log(""+url+ " Allredy exists");
					return;
				}
			}
			root.urls[""+Object.keys(appSettings.urls).length] = url;
			mainLayout.primaryPage.header.message.text = i18n.tr("Added Feed : %1").arg(url);
			mainFeed.updateFeed()
		}
	}

	DispatcherHandler {
			id:dispatcherHandler
	}

	Component.onCompleted:  {
		if(args) {
			console.log("onCompleted args:" + JSON.stringify(args));
			dispatcherHandler.handleOnCompleted(args);
		}
		urssesMainDB.syncUrls();
	}
	
	Timer {
		id:initial
	}
	
	
	Connections {
		target: ContentHub
		
		onShareRequested: {
			if ( transfer.contentType == ContentType.Links ) {
					for ( var i = 0; i < transfer.items.length; i++ ) {
					if (String(transfer.items[i].url).length > 0 ) {
						var url = transfer.items[i].url;
						for(var i in root.urls) {
						if(url == root.urls[i]) {
								console.log(""+url+ " Allredy exists");
								return;
							}
						}
						root.urls[""+Object.keys(appSettings.urls).length] = url;
						mainLayout.primaryPage.header.message.text = i18n.tr("Added Feed : %1").arg(url);
						console.log("Added Feed : " + url);
						mainFeed.updateFeed()
					}
				}
			}
		}
		onImportRequested: {
			opmlImporter.importOPML(transfer);
		}
	}
	
	OpmlImporter {
		id:opmlImporter
	}

}

/*
 * Copyright (C) 2021  Eran DarkEye Uzan
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * darkeye.ursses is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
